$.fn.tabela = function(options){
	
	
	var table = $(this.selector).DataTable( {
    	ajax:{
        	url:options.url,
	        dataSrc:""
        },
    	columns:options.columns
        
    });
	
	$(this.selector + ' tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        } else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        
        $.each(table.row(this).data(),function(key,value) {
            $("#form").find("input[name='"+key+"']").val(value);
        });
        $(".remover").removeClass("hidden");
    });
	
	return table;
}