package org.ufpr.sistemapedidos.services;

import java.io.IOException;
import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.ufpr.sistemapedidos.Pedido;
import org.ufpr.sistemapedidos.dao.DAO;
import org.ufpr.sistemapedidos.dao.PedidoDao;
import org.ufpr.sistemapedidos.validators.CPFValidador;
import org.ufpr.sistemapedidos.validators.PedidoValidador;
import org.ufpr.sistemapedidos.validators.Validador;

@Path("/pedido")
public class PedidoService extends AbstractService<Pedido> {

	@Override
	protected DAO<Pedido> getDAO() {
		return new PedidoDao();
	}

	@Override
	protected Validador<Pedido> getValidador() {
		return new PedidoValidador();
	}

	@GET
	@Path("/{cpf}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscaPorCpf(@PathParam("cpf") String cpf) {
		PedidoDao dao = (PedidoDao) getDAO();
		try {
			(new CPFValidador()).validar(cpf);
			return Response.ok(dao.buscaPorCpf(cpf)).build();
		} catch (RuntimeException | ClassNotFoundException | SQLException | IOException e) {
			return Response.status(500).entity(e).build();
		}
	}
}
