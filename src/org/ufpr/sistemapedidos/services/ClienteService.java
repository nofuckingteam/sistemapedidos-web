package org.ufpr.sistemapedidos.services;

import java.io.IOException;
import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.ufpr.sistemapedidos.Cliente;
import org.ufpr.sistemapedidos.dao.ClienteDao;
import org.ufpr.sistemapedidos.dao.DAO;
import org.ufpr.sistemapedidos.validators.CPFValidador;
import org.ufpr.sistemapedidos.validators.ClienteValidador;
import org.ufpr.sistemapedidos.validators.Validador;

@Path("/cliente")
public class ClienteService extends AbstractService<Cliente>{

	
	@Override
	protected DAO<Cliente> getDAO() {
		return new ClienteDao();
	}

	@Override
	protected Validador<Cliente> getValidador() {
		return new ClienteValidador();
	}
	
	@GET
	@Path("/{cpf}")
	@Produces("application/json")
	public Response buscaPorCpf(@PathParam("cpf") String cpf){
		ClienteDao dao = (ClienteDao) getDAO();
		try {
			(new CPFValidador()).validar(cpf);
			return Response.ok(dao.buscaPorCPF(cpf)).build();
		} catch (RuntimeException | ClassNotFoundException | SQLException | IOException e) {
			return Response.status(500).entity(e).build();
		}
	}
}
