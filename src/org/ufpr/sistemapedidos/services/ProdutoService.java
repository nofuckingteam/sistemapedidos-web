package org.ufpr.sistemapedidos.services;

import javax.ws.rs.Path;

import org.ufpr.sistemapedidos.Produto;
import org.ufpr.sistemapedidos.dao.ProdutoDao;
import org.ufpr.sistemapedidos.dao.DAO;
import org.ufpr.sistemapedidos.validators.ProdutoValidador;
import org.ufpr.sistemapedidos.validators.Validador;

@Path("/produto")
public class ProdutoService extends AbstractService<Produto>{

	@Override
	protected DAO<Produto> getDAO() {
		return new ProdutoDao();
	}

	@Override
	protected Validador<Produto> getValidador() {
		return new ProdutoValidador();
	}

}
