package org.ufpr.sistemapedidos.services;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.ufpr.sistemapedidos.dao.DAO;
import org.ufpr.sistemapedidos.validators.Validador;


public abstract class AbstractService<T> {
	
	protected abstract DAO<T> getDAO();
	protected abstract Validador<T> getValidador();
	
	@GET
	@Path("/")
	@Produces(APPLICATION_JSON)
	public Response buscarTodos(){
		List<T> list;
		try {
			list = getDAO().buscarTodos();
			return Response.ok(list).build();
		} catch (ClassNotFoundException | SQLException | IOException e) {
			e.printStackTrace();
			return Response.status(500).entity(e.getMessage()).build();
		}
	}
	
	@DELETE
	@Path("/")
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	public Response deletar(T t){
		try {
			getDAO().remover(t);
			return Response.ok().build();
		} catch (ClassNotFoundException | SQLException | IOException e) {
			e.printStackTrace();
			return Response.status(500).entity(e.getMessage()).build();
		}
	}
	
	@POST
	@Path("/")
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	public Response inserir(T t){
		try {
			getValidador().validar(t);
			getDAO().inserir(t);
			return Response.status(201).build();
		} catch (RuntimeException | ClassNotFoundException | SQLException | IOException e) {
			return Response.status(500).entity(e).build();
		}
	}
	
	@PUT
	@Path("/")
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	public Response alterar(T t){
		try {
			getValidador().validar(t);
			getDAO().alterar(t);
			return Response.status(201).build();
		} catch (RuntimeException | ClassNotFoundException | SQLException | IOException e) {
			e.printStackTrace();
			return Response.status(500).entity(e.getMessage()).build();
		}
	}
}
